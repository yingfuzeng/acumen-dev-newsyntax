package acumen.interpreters.reference2014

import acumen._
import acumen.Errors._
import acumen.interpreters.semanticstest._

class Reference2014SemanticsTest extends Traditional2014Tests {

  def semantics = SemanticsImpl.Ref2014

}
